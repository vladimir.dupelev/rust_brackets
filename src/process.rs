#[derive(PartialEq)]
enum State {
    Init,
    Number,
}

fn build_str(chars: &mut impl Iterator<Item = char>) -> String {
    let mut n_str = String::from("");
    let mut n: u32;
    let mut result_str = String::from("");
    let mut state = State::Init;

    loop {
        match chars.next() {
            None => {
                return result_str;
            }
            Some(c) => match state {
                State::Init => {
                    if c == ']' {
                        return result_str;
                    } else if c.is_numeric() {
                        n_str.push(c);
                        state = State::Number;
                    } else {
                        result_str.push(c);
                    }
                }
                State::Number => {
                    if c.is_numeric() {
                        n_str.push(c);
                    } else if c == '[' {
                        state = State::Init;
                        n = n_str.parse().unwrap();
                        n_str.clear();

                        let bracket_result = build_str(chars);

                        for _ in 0..n {
                            result_str.push_str(bracket_result.as_str());
                        }
                    }
                }
            },
        }
    }
}

pub fn parse_str(text: &str) -> String {
    let mut chars = text.chars();
    build_str(&mut chars)
}
