mod process;

#[cfg(test)]
mod tests {
    use crate::process::parse_str;

    #[test]
    fn str_text() {
        let input = "abc";
        let result = parse_str(input);
        assert_eq!(result, "abc");
    }

    #[test]
    fn single_test() {
        let input = "1[abc]";
        let result = parse_str(input);
        assert_eq!(result, "abc");
    }

    #[test]
    fn double_test() {
        let input = "2[abc]";
        let result = parse_str(input);
        assert_eq!(result, "abcabc");
    }

    #[test]
    fn ten_test() {
        let input = "10[ab]";
        let result = parse_str(input);
        assert_eq!(result, "abababababababababab");
    }

    #[test]
    fn twotwo_test() {
        let input = "2[ab]2[xy]";
        let result = parse_str(input);
        assert_eq!(result, "ababxyxy");
    }

    #[test]
    fn number_after() {
        let input = "a2[b]";
        let result = parse_str(input);
        assert_eq!(result, "abb");
    }

    #[test]
    fn mix() {
        let input = "2[1[a]2[b]]";
        let result = parse_str(input);
        assert_eq!(result, "abbabb");
    }

    #[test]
    fn mix_advanced() {
        let input = "1[a2[3[c]b]]";
        let result = parse_str(input);
        assert_eq!(result, "acccbcccb");
    }


    #[test]
    fn full() {
        let input = "2[a3[x2[y]]2[b]]";
        let result = parse_str(input);
        assert_eq!(result, "axyyxyyxyybbaxyyxyyxyybb");
    }
}

fn main() {}
